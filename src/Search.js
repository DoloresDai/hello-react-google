import React from 'react';
import './Search.less';
import SearchInput from './SearchInput';
import SearchSubmit from './SearchSubmit'

const Search = () => {
    return (
        <section className='search'>
            <header>
                <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                     alt="Google Logo"/>
            </header>
            <form action="">
                <SearchInput/>
                <SearchSubmit/>
                <p className='language-switch'>Google 提供：<a href="">English</a></p>
            </form>
        </section>
    );
};

export default Search;
