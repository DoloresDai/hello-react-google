import React from 'react';
import {MdKeyboardVoice, MdSearch} from 'react-icons/md';
import './SearchInput.less'

const SearchInput = () => {
    return (
        <ul className="search-input">
            <li><MdSearch className='md-search'/></li>
            <li><input type='text'/></li>
            <li><MdKeyboardVoice className='md-keyboard-voice'/></li>
        </ul>
    );
};

export default SearchInput;
