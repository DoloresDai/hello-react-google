import React from 'react';
import './FooterRight.less';

const FooterRight = () => {
    return (
        <nav className='footer-right'>
            <ul>
                <li><a href="">隐私权</a></li>
                <li><a href="">条款</a></li>
                <li><a href="">设置</a></li>
            </ul>
        </nav>
    );
};

export default FooterRight;
