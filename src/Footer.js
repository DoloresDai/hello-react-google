import React from 'react';
import './Footer.less';
import FooterLeft from './FooterLeft';
import FooterRight from './FooterRight';

const Footer = () => {
    return (
        <footer className='footer'>
            <FooterLeft/>
            <FooterRight/>
        </footer>
    );
};

export default Footer;
