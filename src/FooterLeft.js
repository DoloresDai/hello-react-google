import React from 'react';
import './FooterLeft.less';

const FooterLeft = () => {
    return (
        <nav className='footer-left'>
            <ul>
                <li><a href="">广告</a></li>
                <li><a href="">商务</a></li>
                <li><a href="">Google 大全</a></li>
                <li><a href="">Google 搜索的运作方式</a></li>
            </ul>
        </nav>
    );
};

export default FooterLeft;
