import React from 'react';
import './SearchSubmit.less';

const SearchSubmit = () => {
    return (
        <ul className='search-submit'>
            <li>
                <input type="button" value="Google 搜索" name="google-search"/>
            </li>
            <li>
                <input type="button" value="手气不错" name="good-luck"/>
            </li>
        </ul>
    );
};

export default SearchSubmit;
